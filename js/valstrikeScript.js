$(document).ready(function() {
 
//Searchpart functionality
 $(".searchicon").click(function() {
 	$(".menu").hide();
 	$(".search").show(); 	
 });

 $(".xsign").click(function() {
 	$(".search").hide();
 	$(".menu").show(); 	
 });

//end of searchpart functionality

//Scroll for navigation
$("nav ul li").click(function() {	
	var navItemPos = $(".nav-item").eq($(this).index()).offset().top;
	var pageScrollTime = ($(this).index() * 100) + 500;	
	$("html, body").animate({scrollTop: navItemPos}, pageScrollTime);
});

//end of scroll for navigation

// plugin for truncation texts and putting ellipsis at the end
$(".desc").succinct({size:160});

//end of truncation plugin	

//employee's data on hover
$(".gallery").on({
    mouseenter: function(){
    $(this).children().slice(1,3).show();
    $(this).children("a").children().css("opacity",0.3);
	}, 
	mouseleave: function(){
    $(this).children().slice(1,3).hide();
    $(this).children("a").children().css("opacity",1);
	},
});
//end of employee's data on hover

//work's data on hover
$(".thumb_wrap").on({
    mouseenter: function(){
    $(this).children("h3,div").show();
    $(this).children("img").css("opacity",0.3);
	}, 
	mouseleave: function(){
    $(this).children("h3,div").hide();
    $(this).children("img").css("opacity",1);
    },
});
//end of work's data on hover

//Scroll to top button
$(window).on("scroll", function() {
var z = $(window).scrollTop();
var bt = $("#scroll_button");
	if(z > 50) {
		bt.css("display","block");
	}
	else if (z <= 50) {
		bt.css("display","none");
	}
	if (bt.offset().top >= 555 && bt.offset().top <= 3515) {
		bt.css("background-color","red");
	}
	else {bt.css("background-color","#2c3e50");}
	
});

$("#scroll_button").click(function () {
	$("html, body").animate({scrollTop: "0"},700);
	//$(window).scrollTop() = 0;(this one also works)
});
//end of scroll to top button

}); //end of document ready

//Slider
var image = ["images/slide1.png", "images/slide2.png", "images/slide3.png"];
var imageNumber = 0;
var imageLength = image.length - 1;

function slide(num) {
	imageNumber = imageNumber + num;

	if (imageNumber > imageLength){
		imageNumber = 0;
	}
	if (imageNumber < 0) {
		imageNumber = imageLength;
	}
	if(imageNumber === 0) {
		document.getElementById("detbut").style.display = "block";
		document.getElementById("buybut").style.display = "block";
	}
	else {
		document.getElementById("detbut").style.display = "none";
		document.getElementById("buybut").style.display = "none";
	}

	document.images.namedItem("slides").src = image[imageNumber];
	return false;
}
//end of slider

//Form Validation
function form_val() {
	var firstname = document.getElementById("firstname").value;
	var email_address = document.getElementById("email_address").value;
	var subject = document.getElementById("subject").value;
	var textarea = document.getElementById("textarea").value;

	if(firstname === "" || email_address === "" || subject === "" || textarea === "")	{
		alert("Please fill in all fields");
		return false;
	}
}
// end of form validation